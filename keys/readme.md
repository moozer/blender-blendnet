#myCA pass is 1234



#https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/


# create CA
openssl genrsa -des3 -out myCA.key 2048

# gen root CA
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem


## worker
SERVERNAME=blendnet-worker

# gen worker cert
openssl genrsa -out ${SERVERNAME}.key 2048

# signing request
openssl req -new -key ${SERVERNAME}.key -out ${SERVERNAME}.csr

# create ext file
cat > ${SERVERNAME}.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${SERVERNAME}
EOF

# signing
openssl x509 -req -in ${SERVERNAME}.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial \
-out ${SERVERNAME}.crt -days 825 -sha256 -extfile ${SERVERNAME}.ext
