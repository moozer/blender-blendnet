blender-blendnet
==========================


Warning: defunct project. I keep this for future reference.

The conclusion was that blendnet was not a good solution for video editing - it is relevant for 3D scenes where each frame takes a long time to process.


Yes, We have included CA private keys and other certs. Don't do that! In this case it is ok, since it is only used for internal testing, and on throwaway linux boxes.


To use
-------

we follow https://github.com/state-of-the-art/BlendNet/wiki/HOWTO%3A-Setup-provider%3A-Your-own-render-farm-%28local%29---for-Linux-users
 
To start the worker:
``` 
blender -b -noaudio -P /opt/blendnet/agent.py
```

To start the manager:
```
blender -b -noaudio -P /opt/blendnet/manager.py
```
